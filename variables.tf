variable "bucket-name" {
  description = "Input bucket name default nu.sfile.state"
  type        = string
  default     = "nu.sfile.state"
}

variable "region" {
  description = "Input region default us-east-1"
  type        = string
  default     = "us-east-1"
}

variable "dynamo-table-name" {
  description = "Input dynamodb table name default dynamodb-terraform-state-lock"
  type        = string
  default     = "dynamodb-terraform-state-lock"
}
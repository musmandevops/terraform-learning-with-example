resource "aws_s3_bucket" "state-file-bucket" {
  bucket = var.bucket-name
  acl = "private"
}

resource "aws_s3_bucket" "prod_bucket" {
  bucket = "neustreet.symphony"
  acl    = "public-read"
  policy = file("policy-production.json")

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}
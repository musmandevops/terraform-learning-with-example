output "s3_prod_bucket_website_endpoint" {
  description = "The website endpoint of production app."
  value       = element(concat(aws_s3_bucket.prod_bucket.*.website_endpoint, [""]), 0)
}
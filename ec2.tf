resource "aws_security_group" "sg" {
  name        = "nsinstance"
  description = "Allow TCP/80 & TCP/22"
  ingress {
    description = "Allow SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "nsinstance-key" {
  key_name   = "nsinstance-key"
  public_key = file("./nsinstance-key.pub")
}

resource "aws_instance" "nsinstance" {
  ami                         = "ami-07d02ee1eeb0c996c"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.nsinstance-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  tags = {
    Name = "neustreet-instance"
  }
}


output "nsinstance-Public-IP" {
  value = aws_instance.nsinstance.public_ip
}

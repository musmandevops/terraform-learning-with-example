# We can not use variables in backend that's why giving straight values
terraform {
  backend "s3" {
      encrypt = true
      bucket = "nu.sfile.state" # var can not be used here
      dynamodb_table = "dynamodb-terraform-state-lock"
      key = "terraform.tfstate"
      region = "us-east-1" # var can not be used here
  }
}